package market

type AssetSearchResult struct {
	Name      string `json:"name"`
	Valor     int    `json:"valor"`
	ISIN      string `json:"isin"`
	Url       string `json:"-"`
	AssetType int    `json:"assetType"`
}

type AssetResult struct {
	Name      string  `json:"name"`
	Valor     int     `json:"valor"`
	ISIN      string  `json:"isin"`
	Url       string  `json:"-"`
	AssetType int     `json:"assetType"`
	Price     float64 `json:"price"`
	Currency  string  `json:"currency"`
}
