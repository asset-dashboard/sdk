package market

type Connector interface {
	Search(string) []AssetSearchResult
	Query(string)  AssetResult
	Match(string)  bool
}
