package httpclient

import (
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
)

const (
	USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36"
)

func Get(url string, do func(*http.Response)) error {
	// Create HTTP client with timeout
	client := &http.Client{
		Timeout: 30 * time.Second,
	}

	// Make HTTP GET request
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Warn().Msgf("Error creating reguest: %s", err)
		return err
	}
	// Set user-agent
	request.Header.Set("User-Agent", USER_AGENT)

	// Make request
	log.Debug().Msgf("Requesting: %s", url)
	response, err := client.Do(request)
	if err != nil {
		log.Warn().Msgf("Error doing request to (%s): %s", url, err)
		return err
	}
	defer response.Body.Close()
	log.Debug().Msgf("Response: %s", response.Status)

	// Run callback
	do(response)

	return nil
}
